﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _29_JoshuaAng_PA2_visualstudios {
    public partial class Form1 : Form {
        int FirstNumber, SecondNumber, Sum;
        public Form1() {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e) {
            Application.Exit();  
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            FirstNumber = int.Parse(txt_FirstNumber.Text);
            SecondNumber = int.Parse(txt_SecondNumber.Text);
            Sum = FirstNumber + SecondNumber;
            MessageBox.Show("Sum of two numbers is " + Sum.ToString());

        }
    }
}
